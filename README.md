# IDS721 Spring 2024 Weekly Mini Project 6

## Live Demo

Click this [link](https://zb9tnpw1q4.execute-api.us-east-1.amazonaws.com/default/lambda-logging-tracing/?numbers=3,2,1,1) to access

| Sample output for above url

![img_1.png](img_1.png)

- You could change the number list to sort any random integer list


## Requirements
1. Add logging to a Rust Lambda function
2. Integrate AWS X-Ray tracing
3. Connect logs/traces to CloudWatch

## Setup
1. Install Rust with cargo
2. Install AWS Toolkit for VS Code
3. Install Cargo Lambda `brew tap cargo-lambda/cargo-lambda
   brew install cargo-lambda`

## Run
My simple lambda function takes a random integer number list and returns the sorted list.

1. Create the project `cargo lambda new lambda-demo \
   && cd lambda-demo`
2. Serve the function locally for testing `cargo lambda watch`
3. Test `http://localhost:9000/?numbers=3,1,4,1,5,9,2,6"`
4. Output `{"sorted_numbers":[1,1,2,3,4,5,6,9]}`

## Deploy
1. Deploy the function `cargo lambda deploy`
2. Go to the AWS console and you will find your deployed function

As shown in the screenshot below, the function is deployed and invoked successfully.


## Add logging to a Rust Lambda function

- initialize the tracing subscriber in the main function

```rust
// Initialize the tracing subscriber
tracing_subscriber::fmt()
    .with_env_filter(
        EnvFilter::builder()
            .with_default_directive(LevelFilter::INFO.into())
            .from_env_lossy(),
    )
    .with_target(false)
    .without_time()
    .init();
```

- add INFO level log in the handler function

```rust
// Log the incoming request at INFO level
info!("Received a request: {:?}", event);
```


## Integrate AWS X-Ray tracing

- Enable the X-Ray tracing in the AWS

![img.png](img.png)

- The figure below shows the X-Ray tracing for the lambda function

![img_4.png](img_4.png)

## Connect logs/traces to CloudWatch

- All logs are connected to CloudWatch
![img_2.png](img_2.png)

- The figure below shows the INFO level logs added in my lambda function
![img_3.png](img_3.png)

## Reference

- https://www.cargo-lambda.info/
- https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/welcome.html
